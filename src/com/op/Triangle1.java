package com.op;

public class Triangle1 {
    int a, b, c;
    private int match;

    public Triangle1(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
        match = 0;
        System.out.printf("Side A is %d\n", a);
        System.out.printf("Side B is %d\n", b);
        System.out.printf("Side C is %d\n", c);
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public TriangleType calculate() {
        if (getA() == getB()) {
            match = match + 1;
        }
        if (getA() == getC()) {
            match = match + 2;
        }
        if (getB() == getC()) {
            match = match + 3;
        }
        if (match == 0) {
            if (getA() + getB() <= getC()) {
                return TriangleType.NOT_A_TRIANGLE;
            } else if (getB() + getC() <= getA()) {
                return TriangleType.NOT_A_TRIANGLE;
            } else if (getA() + getC() <= getB()) {
                return TriangleType.NOT_A_TRIANGLE;
            } else {
                return TriangleType.SCALENE;
            }
        } else if (match == 1) {
            if (getA() + getC() <= getB()) {
                return TriangleType.NOT_A_TRIANGLE;
            } else {
                return TriangleType.ISOSCELES;
            }
        } else if (match == 2) {
            if (getA() + getC() <= getB()) {
                return TriangleType.NOT_A_TRIANGLE;
            } else {
                return TriangleType.ISOSCELES;
            }
        } else if (match == 3) {
            if (getB() + getC() <= getA()) {
                return TriangleType.NOT_A_TRIANGLE;
            } else {
                return TriangleType.ISOSCELES;
            }
        } else {
            return TriangleType.EQUILATERAL;
        }
    }
}
