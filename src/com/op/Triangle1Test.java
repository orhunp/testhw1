package com.op;

import static org.junit.jupiter.api.Assertions.*;

class Triangle1Test {

    @org.junit.jupiter.api.Test
    void testNotATriangle() {
        assertEquals(TriangleType.NOT_A_TRIANGLE, new Triangle1(3, 4, 7).calculate());
        assertEquals(TriangleType.NOT_A_TRIANGLE, new Triangle1(7, 3, 4).calculate());
        assertEquals(TriangleType.NOT_A_TRIANGLE, new Triangle1(3, 7, 4).calculate());
        assertEquals(TriangleType.NOT_A_TRIANGLE, new Triangle1(5, 5, 0).calculate());
        assertEquals(TriangleType.NOT_A_TRIANGLE, new Triangle1(5, 10, 5).calculate());
        assertEquals(TriangleType.NOT_A_TRIANGLE, new Triangle1(10, 5, 5).calculate());
    }

    @org.junit.jupiter.api.Test
    void testScalene() {
        assertEquals(TriangleType.SCALENE, new Triangle1(3, 4, 5).calculate());
        assertEquals(TriangleType.SCALENE, new Triangle1(40, 30, 50).calculate());
        assertEquals(TriangleType.SCALENE, new Triangle1(5, 12, 13).calculate());
        assertEquals(TriangleType.SCALENE, new Triangle1(7, 24, 25).calculate());
    }

    @org.junit.jupiter.api.Test
    void testIsosceles() {
        assertEquals(TriangleType.ISOSCELES, new Triangle1(6, 6, 8).calculate());
        assertEquals(TriangleType.ISOSCELES, new Triangle1(3, 3, 4).calculate());
        assertEquals(TriangleType.ISOSCELES, new Triangle1(5, 6, 5).calculate());
        assertEquals(TriangleType.ISOSCELES, new Triangle1(3, 3, 2).calculate());
        assertEquals(TriangleType.ISOSCELES, new Triangle1(9, 5, 5).calculate());
    }

    @org.junit.jupiter.api.Test
    void testEquilateral() {
        assertEquals(TriangleType.EQUILATERAL, new Triangle1(10, 10, 10).calculate());
        assertEquals(TriangleType.EQUILATERAL, new Triangle1(30, 30, 30).calculate());
        assertEquals(TriangleType.EQUILATERAL, new Triangle1(80, 80, 80).calculate());
    }
}
