package com.op;

public enum TriangleType {
    NOT_A_TRIANGLE,
    SCALENE,
    ISOSCELES,
    EQUILATERAL,
}
